<?php
include_once('../../vendor/autoload.php');
use App\Student\Student;

$student=new Student();
$student->prepare($_GET);
$singleItem=$student->view();
//\App\Student\Utility::dd($singleItem);
?>

<!DOCTYPE html>

<html lang="en">

<head>
    <title>Student Name Insert</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <center><h2>Create Student Name</h2></center>
    <a href="index.php" class="btn btn-primary">Back To List</a>
    <br><br>
    <div class="form-group">
        <form role="form"  action="update.php">

            <br><br>

            <input type="hidden" name="id" value="<?php echo $singleItem['id']?>">
            <input type="text" class ="form-control" name="firstname" id="student"  placeholder="<?php echo $singleItem['firstname']?>">
            <br>
            <input type="text" class ="form-control" name="middlename" id="student"  value="<?php echo $singleItem['middlename']?>">
            <br>
            <input type="text" class ="form-control" name="lastname" id="student" value="<?php echo $singleItem['lastname']?>">
            <br><br>
            <button class="btn btn-primary" role="button" value="submit">Submit</button>
        </form>
    </div>

</div>


</body>
</html>
