<?php

include_once('../../vendor/autoload.php');
use App\Student\Student;
use App\Student\Utility;

$student=new Student();
$student->prepare($_GET);
$singleItem=$student->view();
//Utility::dd($singleItem);
?>

<!DOCTYPE html>

<html lang="en">

<head>
    <title>Student Name Insert</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <center><h2>View Student Name</h2></center>

    <a href="index.php" class="btn btn-primary">Back To List</a>
    <br><br>
    <div class="list-group">


            <br><br>


        <ul>
            <li class="list-group-item">ID:<?php echo $singleItem["id"]?></li>
            <li class="list-group-item">First name:<?php echo " ".$singleItem["firstname"]?></li>
            <li class="list-group-item">Middle Name:<?php echo " ".$singleItem["middlename"]?></li>
            <li class="list-group-item">Last Name:<?php echo " ".$singleItem["lastname"]?></li>


            <br><br>
</ul>
    </div>

</div>


</body>
</html>
