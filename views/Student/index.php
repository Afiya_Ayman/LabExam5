<?php
include_once('../../vendor/autoload.php');
use App\Student\Student;
use App\Student\Utility;

$student=new Student();
//$student->prepare($_GET);
$allStudent=$student->index();
//Utility::dd($allStudent);

?>

<!DOCTYPE html>

<html lang="en">

<head>
    <title>Student Name List</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../../Resource/css/bootstrap.min.css">
</head>

<body>

<div class="container">
    <center><h2>List of Student Name</h2></center>
    <br><br>
    <a href="create.php" class="btn btn-primary">Insert Again</a>
    <br><br>
    <div class="table-responsive">
    <table class="table">
        <thead>
        <tr>

            <td><strong>SL</strong></td>
            <td><strong>ID</strong></td>
            <td><strong>First Name</strong></td>
            <td><strong>Middle Name</strong></td>
            <td><strong>Last Name</strong></td>
            <td><strong>Action</strong></td>

        </tr>
        </thead>

        <tbody>
        <?php
        $sl=1;
        foreach ($allStudent as $student)
        {
        ?>
        <tr>
            <td><?php echo $sl++;?></td>
            <td><?php echo $student['id']?></td>
            <td><?php echo $student['firstname']?></td>
            <td><?php echo $student['middlename']?></td>
            <td><?php echo $student['lastname']?></td>

            <td>
            <a href="view.php?id=<?php echo $student['id']?>" class="btn btn-info">View</a>
                <a href="edit.php?id=<?php echo $student['id']?>" class="btn btn-primary">Edit</a>
                <a href="delete.php?id=<?php echo $student['id']?>" class="btn btn-danger">Delete</a>
            </td>

        </tr>
        <?php }
        ?>
        </tbody>
        </table>
        </div>

</div>


</body>
</html>
