<?php
namespace App\Student;

class Utility
{
    public static function dd($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        die();
    }
    public static function redirect($data)
    {

        header('Location:'.$data);
    }
}