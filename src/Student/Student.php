<?php
namespace App\Student;
class Student
{

    public $id;
    public $firstName;
    public $middleName;
    public $lastName;
    public $conn;

    public function __construct()
    {
        $this->conn=mysqli_connect("localhost","root","","labxm5b22") or die("Database Connection failed");
    }

    public function prepare($data="")
    {
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }
        if (array_key_exists('firstname',$data))
        {
            $this->firstName=$data['firstname'];
        }
        if (array_key_exists('middlename',$data))
        {
            $this->middleName=$data['middlename'];
        }
        if (array_key_exists('lastname',$data))
        {
            $this->lastName=$data['lastname'];
        }
    }

    public function store()
    {
        $query="INSERT INTO `labxm5b22`.`student` (`firstname`, `middlename`, `lastname`) VALUES ('".$this->firstName."', '".$this->middleName."', '".$this->lastName."')";
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Successfully Inserted!";
        }else{
            echo "Error!";
        }

    }

    public function index()
    {
        $_allName=array();
        $query="SELECT * FROM `student`";
        $result=mysqli_query($this->conn,$query);
        while ($row=mysqli_fetch_assoc($result))
        {
            $_allName[]=$row;
        }
        return $_allName;
        //var_dump($_allName);
    }

    public function view()
    {
        $query="SELECT * FROM `student` WHERE `student`.`id`=".$this->id;
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        $row=mysqli_fetch_assoc($result);
        return $row;
    }
    public function update()
    {
        $query="UPDATE `labxm5b22`.`student` SET `firstname` = '".$this->firstName."', `middlename` = '".$this->middleName."', `lastname` = '".$this->lastName."' WHERE `student`.`id` = ".$this->id;
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Successfully Update!";
        }else{
            echo "Error!";
        }

    }

    public function delete()
    {
        $query="DELETE FROM `labxm5b22`.`student` WHERE `student`.`id` = ".$this->id;
        //echo $query;
        $result=mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Successfully Deleted!";
        }else{
            echo "Error!";
        }

    }



}