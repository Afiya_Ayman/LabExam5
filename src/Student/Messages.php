<?php

namespace App\Student;

class Message
{
    public static function message($message=NULL)
    {
        if(is_null($message))
        {
            $_message=self::getMessage();
        }else
        {
            self::setMessage($message);
        }
    }
    public static function getMessage()
    {
        $_message=$_SESSION['message'];
        return $_message;
    }
    public static function setMessage($message)
    {
        $_message=$_SESSION['message'];
        $_SESSION['message']="";
        return $_SESSION['message'];

    }
}